# Ludum Dare 38 (2017) - The Grass is Always Greener

## Building

		python -m venv _env
		source ./_env/bin/activate
		pip install -r requirements.txt
		pyinstaller ./game.py


## Running

		python ./game.py

or

		./dist/game/game


