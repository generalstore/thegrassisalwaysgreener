altgraph==0.17.4
packaging==24.0
pygame==2.5.2
pyinstaller==6.6.0
pyinstaller-hooks-contrib==2024.4
